<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

    Route::group(['namespace' => 'App\Http\Controllers'], function () {
      Route::post('register', 'UserController@register');
      Route::post('login', 'UserController@authenticate');
      Route::get('open', 'DataController@open');
    });


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'App\Http\Controllers'], function() {
Route::get('test', 'iventCreator@callApi');
Route::post('giveAll', 'iventCreator@indexAll');
Route::post('giveAllWhere', 'iventCreator@IndexOneEvent');
Route::post('giveWhereIndex', 'iventCreator@index');
});
